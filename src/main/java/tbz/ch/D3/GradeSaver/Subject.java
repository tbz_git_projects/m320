/*
 * Copyright (c) 2023-2023. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package tbz.ch.D3.GradeSaver;

import java.math.BigDecimal;
import java.util.ArrayList;

public class Subject {
    private String name;
    private ArrayList<Double> grades; // Create an ArrayList object

    public Subject(String name, ArrayList<Double> grades) {
        this.name = name;
        this.grades = grades;
    }

    public void PrintSubject(){
        StringBuilder SubjectPrint = new StringBuilder();
        System.out.println("\n"+getName().toUpperCase()+": ");
        System.out.println("Subject: " + getName());
        SubjectPrint.append("Grades: ");
        for (double grade : getGrades()) {
            SubjectPrint.append(grade).append(", ");
        }
        System.out.println(SubjectPrint);
        System.out.println("Grade Average: " + getGradeAverage()+"\n");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Double> getGrades() {
        return grades;
    }

    public void addGrade(double grade) {
        grades.add(grade);
    }

    public void addGrades(ArrayList<Double> grades) {
        this.grades.addAll(grades);
    }

    public void removeGrade(double grade) {
        grades.remove(grade);
    }

    public void removeGrades(ArrayList<Double> grades) {
        this.grades.removeAll(grades);
    }

    public double getGradeAverage() {
        float total = 0;
        for (double grade: grades) {
            total += grade;
        }
        double gradeAverage = total / grades.size();
        BigDecimal bigDecimal = BigDecimal.valueOf(gradeAverage);
        gradeAverage = bigDecimal.doubleValue();
        return gradeAverage;
    }


}
