package tbz.ch.V3_StockExchange;

import java.util.ArrayList;
import java.util.Objects;


public class BoerseNY implements StockExchange {

    ArrayList<Aktie> Aktien = new ArrayList<Aktie>();
    public BoerseNY(){
        Aktien.add(new Aktie(200, "MircoSoft2"));
        Aktien.add(new Aktie(210, "BBC2"));
        Aktien.add(new Aktie(220, "Sowatec2"));
        Aktien.add(new Aktie(230, "TBZ2"));
        Aktien.add(new Aktie(240, "HB2"));


    }
    @Override
    public void showStockExchange() {
        for (Aktie aktie : Aktien) {
            System.out.print("BNY: ");
            System.out.print(aktie.Name + " ");
            System.out.println(aktie.Wert + "");
        }
    }

    @Override
    public void updatePrice(String name, int newPrice) {
        for (Aktie aktie : Aktien) {
            if (Objects.equals(aktie.Name, name))
            aktie.Wert = newPrice;
        }
    }
}
