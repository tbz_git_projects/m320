package tbz.ch.Q3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tbz.ch.Q3.Friendships;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


class FriendshipsTest {

    //have the necessary attributes listed:
    Friendships friendships;


    @BeforeEach
    void setUp() throws Exception {
        // set up the objects with dummy data
        friendships = new Friendships();
        friendships.makeFriends("Joe", "Biden");
        friendships.makeFriends("Joe", "MAMA");

    }

    @AfterEach
    void tearDown() throws Exception {
        //this method is executed after testing
        //useful to close external resources if necessary
    }


    @Test
    void testGetFriendsList() {
        assertEquals(2, friendships.getFriendsList("Joe"), 0);

    }

    @Test
    void testAreFriends() {
        assertTrue(friendships.areFriends("Joe", "Biden"));
    }

}
