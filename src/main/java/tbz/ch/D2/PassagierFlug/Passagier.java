package tbz.ch.D2.PassagierFlug;

public class Passagier {
    String name;

    public Passagier(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
