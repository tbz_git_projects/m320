/*
 * Copyright (c) 2023-2023. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package tbz.ch.D4_V4.Roles.FighterRole;

import tbz.ch.D4_V4.StatsType;
import tbz.ch.D4_V4.Weapons.Weapon;

public class SwashBuckler extends FighterRole {
    String name = "SwashBuckler";

    public SwashBuckler(Weapon weapon) {
        super(weapon);
        super.setCharacterClassName(name);
    }
    @Override
    public StatsType getTypeofClassBonus() {
        return StatsType.ANGUEL;
    }
}
