package tbz.ch.V3_StockExchange;

import java.util.ArrayList;
import java.util.Objects;


public class BoerseLondon implements StockExchange {

    ArrayList<Aktie> Aktien = new ArrayList<Aktie>();
    public BoerseLondon(){
        Aktien.add(new Aktie(300, "MircoSoft3"));
        Aktien.add(new Aktie(310, "BBC3"));
        Aktien.add(new Aktie(320, "Sowatec3"));
        Aktien.add(new Aktie(330, "TBZ3"));
        Aktien.add(new Aktie(340, "HB3"));


    }
    @Override
    public void showStockExchange() {
        for (Aktie aktie : Aktien) {
            System.out.print("BL: ");
            System.out.print(aktie.Name + " ");
            System.out.println(aktie.Wert + "");
        }
    }

    @Override
    public void updatePrice(String name, int newPrice) {
        for (Aktie aktie : Aktien) {
            if (Objects.equals(aktie.Name, name))
            aktie.Wert = newPrice;
        }
    }
}
