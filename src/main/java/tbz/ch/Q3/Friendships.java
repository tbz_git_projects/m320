package tbz.ch.Q3;/*
 * Copyright (c) 2023. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */


import java.util.ArrayList;

public class Friendships {

    ArrayList<String> friendships = new ArrayList<>();
    ArrayList<String> friends = new ArrayList<>();

    public void makeFriends(String name, String otherName) {

        friends.add(name);
        friends.add(otherName);
        friendships.add(String.valueOf(friends));
    }

    public int getFriendsList(String name) {

        return friendships.size();
    }

    public boolean areFriends(String person, String friend) {

        return friends.contains(person) && friends.contains(friend);

    }
}
