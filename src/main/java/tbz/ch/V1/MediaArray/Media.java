package tbz.ch.V1.MediaArray;

public abstract class Media {

    public Media(String title, String year, double price) {
        Title = title;
        Year = year;
        Price = price;
    }

    protected String Title;
    protected String Year;
    protected double Price;

}
