package tbz.ch.V3_StockExchange;

public class Main {

    public static void main(String[] args) {

        BoerseZuerich boerseZuerich = new BoerseZuerich();
        Portfolio portfolioZuerich = new Portfolio(boerseZuerich);
        System.out.println("\nboerseZuerich:");
        portfolioZuerich.Boerse.showStockExchange();

        BoerseNY boerseNY = new BoerseNY();
        Portfolio portfolioNewYork = new Portfolio(boerseNY);
        System.out.println("\nboerseNY:");
        portfolioNewYork.Boerse.showStockExchange();

        BoerseLondon boerseLondon = new BoerseLondon();
        Portfolio portfolioLondon = new Portfolio(boerseLondon);
        System.out.println("\nboerseLondon:");
        portfolioLondon.Boerse.showStockExchange();

        System.out.println("\nUpdate Price:");
        portfolioNewYork.Boerse.updatePrice("MircoSoft2", 69);
        portfolioLondon.Boerse.updatePrice("BBC3", 420);

        System.out.println("\nboerseNY:");
        portfolioNewYork.Boerse.showStockExchange();
        System.out.println("\nportfolioLondon:");
        portfolioLondon.Boerse.showStockExchange();

    }
}
