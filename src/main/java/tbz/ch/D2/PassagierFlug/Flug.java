package tbz.ch.D2.PassagierFlug;

import java.util.ArrayList;
import java.util.List;

public class Flug {
    List<Passagier> passagiere = new ArrayList<>();

    void PassagierListeAusgeben() {
        System.out.println("Passagierliste:");
        for (Passagier p: passagiere) {
            System.out.println(p.getName());
        }
    }

    public void addPassagiere(Passagier passagiere) {
        this.passagiere.add(passagiere);
    }
}
