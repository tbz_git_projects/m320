/*
 * Copyright (c) 2022-2023. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package tbz.ch.D3.GradeSaver;

import java.util.ArrayList;
import java.util.Scanner;

public class GradeSaver {


    public static void main(String[] args) {
        ArrayList<Subject> subjects = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        boolean runProgram = true;

        System.out.println("old or new");
        String answer = sc.nextLine();
        System.out.println("Welcome to GradeSaver,");
        if (answer.equals("o")) {
            ArrayList<Double> mGrades = new ArrayList<>();
            mGrades.add(3.4);
            mGrades.add(4.0);
            mGrades.add(3.0);

            subjects.add(new Subject("math", mGrades));
            ArrayList<Double> oopGrades = new ArrayList<>();
            oopGrades.add(6.0);
            oopGrades.add(5.0);
            oopGrades.add(5.5);

            subjects.add(new Subject("OOP", oopGrades));
        } else {
            do {
                subjects.add(CreateSubject(sc));
                System.out.println("Wanna add another Subject n/Y");
                answer = sc.nextLine();
            } while (!answer.equals("n"));
        }
        printSubjects(subjects);
        while (runProgram) {
            System.out.println("""
                    Welcome to GradeCalco, What you wanna do ?
                    Press the number according to the selections>
                    1. Show all your Subject and their Grades
                    2. Show Grades from One Subject
                    3. Add or Remove a grade from a subject
                    4. add or remove a Subject with their grades
                    0  Exit the Program""");
            answer = sc.nextLine();
            switch (answer) {
                case "1" -> printSubjects(subjects);
                case "2" -> ChooseSubject(2, subjects, sc);
                case "3" -> ChooseSubject(3, subjects, sc);
                case "4" -> ChooseSubject(4, subjects, sc);
                case "0" -> runProgram = false;
                default -> System.out.println("There no option like that");
            }
        }
    }

    private static void ChooseSubject(int chosseenNumber, ArrayList<Subject> subjects, Scanner sc) {

        for (int i = 0; i < subjects.size(); i++) {
            System.out.println(i + ". " + subjects.get(i).getName());
        }
        System.out.println("Choose the number the for Selection");
        int answer = Integer.parseInt(sc.nextLine());
        if (chosseenNumber == 2) {
            subjects.get(answer).PrintSubject();
        } else if (chosseenNumber == 3) {
            ModifySubjects(subjects, sc, answer);
        } else {
            ModifyGradesFromSubject(subjects, sc, answer);
        }
    }

    private static void ModifySubjects(ArrayList<Subject> subjects, Scanner sc, int answer) {
        System.out.println("Add or RM?");
        String addOrRemove = sc.nextLine();
        if (addOrRemove.equals("Add")) {
            String a;
            do {
                subjects.add(CreateSubject(sc));
                System.out.println("Wanna add another Subject n/Y");
                a = sc.nextLine();
            } while (!a.equals("n"));
        } else {
            System.out.println("Do you want to delete \"" + subjects.get(answer).getName() + "\" Y/n?");
            String conform = sc.nextLine();
            if (conform.equals("Y")) {
                subjects.remove(answer);
            }
        }
    }

    private static void ModifyGradesFromSubject(ArrayList<Subject> subjects, Scanner sc, int answer) {
        Subject subject = subjects.get(answer);
        int answerGrade;
        System.out.println("Add or RM?");
        String addOrRemove = sc.nextLine();
        if (addOrRemove.equals("Add")) {
            System.out.println("Please type your Grade");
            answerGrade = Integer.parseInt(sc.nextLine());
            subject.addGrade(answerGrade);
        } else {
            subject.PrintSubject();
            for (int i = 0; i < subject.getGrades().size(); i++) {
                System.out.println(i + ". " + subject.getGrades().get(i));
            }
            System.out.println("Choose the number the for Selection");
            answerGrade = Integer.parseInt(sc.nextLine());
            System.out.println("Do you want to delete " + subject.getGrades().get(answerGrade) + " Y/n");
            String conform = sc.nextLine();
            if (conform.equals("Y")) {
                subjects.remove(answer);
            }
        }
    }

    private static Subject CreateSubject(Scanner sc) {
        System.out.println("Please Add a Subject name");
        String subjectName = sc.nextLine();
        ArrayList<Double> grades = new ArrayList<>();
        while (true) {
            System.out.println("Please add a Grade");
            double grade = Float.parseFloat(sc.nextLine());
            grades.add(grade);
            System.out.println("Wanna add another Grade n/Y");
            String answer = sc.nextLine();
            if (answer.equals("n")) {
                break;
            }
        }
        return new Subject(subjectName, grades);

    }


    private static void printSubjects(ArrayList<Subject> subjects) {
        for (Subject subject : subjects) {
            subject.PrintSubject();
        }
    }
}
