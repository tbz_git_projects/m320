package tbz.ch.D1.BankSchalter;


public class Konto {

    private int kontoNr;
    private double saldo;


    public int getKontoNr() {
        return kontoNr;
    }

    public void setKontoNr(int kontoNr) {
        this.kontoNr = kontoNr;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public void einzahlen(double money) {
        saldo += money;
    }

    public void abheben(double money) {


        if (saldo - money > 0) {
            saldo = saldo - money;

        } else {
            System.out.println("Nicht genügend Geld zum Auszahlen");
        }
    }
}
