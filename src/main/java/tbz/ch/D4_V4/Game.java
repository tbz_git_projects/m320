package tbz.ch.D4_V4;


import tbz.ch.D4_V4.Race.Human;
import tbz.ch.D4_V4.Roles.FighterRole.Samurai;
import tbz.ch.D4_V4.Roles.FighterRole.SwashBuckler;
import tbz.ch.D4_V4.Roles.FighterRole.Warlord;
import tbz.ch.D4_V4.Roles.MagicianRole.Druid;
import tbz.ch.D4_V4.Roles.MagicianRole.Necromancer;
import tbz.ch.D4_V4.Roles.MagicianRole.Trickster;
import tbz.ch.D4_V4.Roles.RangerRole.Gunner;
import tbz.ch.D4_V4.Roles.RangerRole.Hunter;
import tbz.ch.D4_V4.Roles.RangerRole.Sniper;
import tbz.ch.D4_V4.Roles.Role;
import tbz.ch.D4_V4.Weapons.MagicWeapon.Leaf;
import tbz.ch.D4_V4.Weapons.MagicWeapon.MagicGun;
import tbz.ch.D4_V4.Weapons.MagicWeapon.Skull;
import tbz.ch.D4_V4.Weapons.MeleeWeapon.HockSword;
import tbz.ch.D4_V4.Weapons.MeleeWeapon.Katana;
import tbz.ch.D4_V4.Weapons.MeleeWeapon.Spear;
import tbz.ch.D4_V4.Weapons.RangedWeapon.Boomerang;
import tbz.ch.D4_V4.Weapons.RangedWeapon.Crossbow;
import tbz.ch.D4_V4.Weapons.RangedWeapon.Gun;
import tbz.ch.D4_V4.Weapons.Weapon;

import java.util.ArrayList;
import java.util.Scanner;

public class Game {
    Human player;
    int standardStats = 10;
    Scanner sc = new Scanner(System.in);
    ArrayList<Role> allClasses = new ArrayList<>();
    ArrayList<ArrayList<Role>> allClasses2D = new ArrayList<>();
    ArrayList<Role> allRoles = new ArrayList<>();
    ArrayList<Weapon> allWeapons = new ArrayList<>();

    public Game() {

        ArrayList<Role> allFighterClasses = new ArrayList<>();
        ArrayList<Role> allRangerClasses = new ArrayList<>();
        ArrayList<Role> allMagicianClasses = new ArrayList<>();
        allFighterClasses.add(new SwashBuckler(new HockSword()));
        allFighterClasses.add(new Warlord(new Spear()));
        allFighterClasses.add(new Samurai(new Katana()));
        allRangerClasses.add(new Gunner(new Gun()));
        allRangerClasses.add(new Hunter(new Crossbow()));
        allRangerClasses.add(new Sniper(new Boomerang()));
        allMagicianClasses.add(new Trickster(new MagicGun()));
        allMagicianClasses.add(new Necromancer(new Skull()));
        allMagicianClasses.add(new Druid(new Leaf()));

        allClasses2D.add(allFighterClasses);
        allClasses2D.add(allMagicianClasses);
        allClasses2D.add(allRangerClasses);


        allRoles.add(new SwashBuckler(new HockSword()));
        allRoles.add(new Gunner(new Gun()));
        allRoles.add(new Trickster(new MagicGun()));

        allClasses.add(new SwashBuckler(new HockSword()));
        allClasses.add(new Warlord(new Spear()));
        allClasses.add(new Samurai(new Katana()));
        allClasses.add(new Gunner(new Gun()));
        allClasses.add(new Hunter(new Crossbow()));
        allClasses.add(new Sniper(new Boomerang()));
        allClasses.add(new Trickster(new MagicGun()));
        allClasses.add(new Necromancer(new Skull()));
        allClasses.add(new Druid(new Leaf()));

        allWeapons.add(new HockSword());
        allWeapons.add(new Spear());
        allWeapons.add(new Katana());
        allWeapons.add(new Gun());
        allWeapons.add(new Crossbow());
        allWeapons.add(new Boomerang());
        allWeapons.add(new MagicGun());
        allWeapons.add(new Skull());
        allWeapons.add(new Leaf());

    }

    void start() {
        System.out.println("Hello new User, what is your name?");
        String name = sc.nextLine();
        System.out.println("Hello " + name);
        Role ChosenClass = classSelection();

        player = new Human(name, standardStats, standardStats, standardStats, standardStats, ChosenClass);
        giveBonuses(ChosenClass);
        player.showProfile();
    }

    private void giveBonuses(Role chosenClass) {
        switch (chosenClass.getTypeofRoleBonus()) {
            case STRENG -> player.addStreng(10);
            case MANUE -> player.addManoe(10);
            case ANGUEL -> player.addAnguel(10);
            case HELF -> player.addHelf(10);
        }
        switch (chosenClass.getTypeofClassBonus()) {
            case STRENG -> player.addStreng(5);
            case MANUE -> player.addManoe(5);
            case ANGUEL -> player.addAnguel(5);
            case HELF -> player.addHelf(5);
        }
    }

    private Role classSelection() {
        Role chosenClass = (choseClass(choseRole()));
        chosenClass.setWeapon(choseWeapon());
        return chosenClass;
    }

    private Weapon choseWeapon() {
        while (true) {
            System.out.println("Please choose your Weapon,\nwrite the number to the role");
            for (int i = 0; i < allWeapons.size(); i++) {
                System.out.println(i + "." + allWeapons.get(i).getName());
            }
            int chosenWeapon = Integer.parseInt(sc.nextLine());
            if (CheckOption(chosenWeapon, allWeapons)) return allWeapons.get(chosenWeapon);
        }
    }

    private Role choseClass(String chosenRole) {
        while (true) {
            System.out.println("Please choose your class,\nwrite the number to the role");
            for (int i = 0; i < allClasses.size(); i++) {
                if (allClasses.get(i).getRoleName().equals(chosenRole))
                    System.out.println(i + "." + allClasses.get(i).getName());
            }
            int chosenClass = Integer.parseInt(sc.nextLine());
            if (CheckOption(chosenClass, allClasses)) return allClasses.get(chosenClass);
        }
    }

    private String choseRole() {
        while (true) {
            System.out.println("Please choose your role,\nwrite the number to the role");
            for (int i = 0; i < allRoles.size(); i++) {
                System.out.println(i + "." + allRoles.get(i).getRoleName());
            }
            int chosenRole = Integer.parseInt(sc.nextLine());
            if (CheckOption(chosenRole, allRoles)) return allRoles.get(chosenRole).getRoleName();
        }
    }

    private boolean CheckOption(int answer, ArrayList arrayList) {
        try {
            if (arrayList.contains(arrayList.get(answer))) {
                return true;
            }
        } catch (Exception e) {
            System.out.println("User input couldn't be processed try something else ");
        }
        return false;
    }
}

