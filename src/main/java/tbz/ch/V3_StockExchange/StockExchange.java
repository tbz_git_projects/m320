package tbz.ch.V3_StockExchange;

public interface StockExchange {

    void showStockExchange();
    void updatePrice(String name, int newPrice);

}
