package tbz.ch.V2.FussBallManschaft;

public class Goalie extends Spieler{
    private double koerperGroesse;

    public double getKoerperGroesse() {
        return koerperGroesse;
    }

    public void setKoerperGroesse(double koerperGroesse) {
        this.koerperGroesse = koerperGroesse;
    }
    @Override
    public void spielen(){
        System.out.println("Probiert ein Goal zu verhindern");
    }
}
