/*
 * Copyright (c) 2023-2023. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package tbz.ch.D4_V4.Race;

import tbz.ch.D4_V4.Roles.Role;


public class Human extends Race {

    Role CharacterClass;

    public Human(String name, int helf, int manoe, int streng, int anguel, Role characterClass) {
        super(name, helf, manoe, streng, anguel);
        CharacterClass = characterClass;
    }

    public Role getCharacterClass() {
        return CharacterClass;
    }

    public void setCharacterClass(Role characterClass) {
        CharacterClass = characterClass;
    }

    public void showProfile() {
        System.out.println("\n********************************" +
                "\nName: " + getName() + "" +
                "\nHealth: " + getHelf() +
                "\nMana: " + getManoe() +
                "\nStrength: " + getStreng() +
                "\nAgile: " + getAnguel() +
                "\nRole: " + getCharacterClass().getRoleName() +
                "\nClass: " + getCharacterClass().getName() +
                "\nWeapon: " + getCharacterClass().getWeapon().getName() +
                "\n******************************");
    }
}
