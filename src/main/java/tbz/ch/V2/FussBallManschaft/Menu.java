package tbz.ch.V2.FussBallManschaft;

public class Menu {

    public static void main(String[] args) {
        Mannschaft myMannschaft = new Mannschaft();
        for (Spieler spieler:myMannschaft.spieler) {
            spieler.spielen();
        }
    }
}
