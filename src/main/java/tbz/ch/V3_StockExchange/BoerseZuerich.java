package tbz.ch.V3_StockExchange;

import java.util.ArrayList;
import java.util.Objects;


public class BoerseZuerich implements StockExchange {

    ArrayList<Aktie> Aktien = new ArrayList<Aktie>();
    public BoerseZuerich(){
        Aktien.add(new Aktie(100, "MircoSoft"));
        Aktien.add(new Aktie(110, "BBC"));
        Aktien.add(new Aktie(120, "Sowatec"));
        Aktien.add(new Aktie(130, "TBZ"));
        Aktien.add(new Aktie(140, "HB"));


    }
    @Override
    public void showStockExchange() {
        for (Aktie aktie : Aktien) {
            System.out.print("BZ: ");
            System.out.print(aktie.Name + " ");
            System.out.println(aktie.Wert + "");
        }
    }

    @Override
    public void updatePrice(String name, int newPrice) {
        for (Aktie aktie : Aktien) {
            if (Objects.equals(aktie.Name, name))
            aktie.Wert = newPrice;
        }
    }
}
