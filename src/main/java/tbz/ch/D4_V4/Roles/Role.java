/*
 * Copyright (c) 2023-2023. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package tbz.ch.D4_V4.Roles;

import tbz.ch.D4_V4.StatsType;
import tbz.ch.D4_V4.Weapons.Weapon;

public abstract class Role implements RoleBehaviour {

    String roleName;
    String CharacterClassName;
    Weapon weapon;

    public Role(Weapon weapon) {
        this.weapon = weapon;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String characterClassName) {
        this.roleName = characterClassName;
    }

    public String getName() {
        return CharacterClassName;
    }

    public void setCharacterClassName(String characterClassName) {
        this.CharacterClassName = characterClassName;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public abstract StatsType getTypeofRoleBonus();
}
