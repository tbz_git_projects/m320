package tbz.ch.D1.BankSchalter;

import java.util.Scanner;

public class Bankschalter {

    public static void main(String[] args) {

        boolean duration = true;

        Konto konto = new Konto();
        Konto konto1 = new Konto();
        konto1.setKontoNr(1);
        konto1.setSaldo(1000);
        Konto konto2 = new Konto();
        konto2.setKontoNr(2);
        konto2.setSaldo(40000);
        Konto konto3 = new Konto();
        konto3.setKontoNr(3);
        konto3.setSaldo(7000);

        Scanner scan = new Scanner(System.in);

        System.out.println("Willkommen zu Bank!\n");
        System.out.println("Loggen sie sich mit Ihrem Konto an (1 / 2 / 3)");

        switch(scan.nextInt()) {
            case 1:
                konto = konto1;
                break;
            case 2:
                konto = konto2;
                break;
            case 3:
                konto = konto3;
                break;
            default:
                break;
        }


        while (duration) {

            System.out.println("Was wollen Sie machen? (0) Kontostand (1) Einzahlen / (2) Auszahlen / (3) Transaktion / (4) Exit");
            int choice = scan.nextInt();

            switch(choice) {
                case 0: //Kontostand
                    System.out.println("Kontostand: " + konto.getSaldo());
                    break;
                case 1: //Einzahlen
                    System.out.println("Kontostand: " + konto.getSaldo() + "\n");
                    System.out.println("Geben Sie Ihren Betrag ein: ");
                    konto.einzahlen(scan.nextDouble());
                    System.out.println("Kontostand: " + konto.getSaldo());
                    break;
                case 2: //Auszahlen
                    System.out.println("Kontostand: " + konto.getSaldo());
                    System.out.println("Geben Sie Ihren Betrag ein: ");
                    konto.abheben(scan.nextDouble());
                    System.out.println("Kontostand: " + konto.getSaldo());
                    break;
                case 3: //Transaktion
                    System.out.println("Wo wollen sie das Geld transferieren? KontoNr: ");
                    int transferNr = scan.nextInt();
                    System.out.println("Wie viel Geld wollen Sie transferieren? Saldo: ");
                    double money = scan.nextDouble();

                    konto.abheben(money);

                    if (konto.getKontoNr() != transferNr && transferNr == konto1.getKontoNr()) {
                        konto1.einzahlen(money);
                        System.out.println(money + " CHF an Konto " + konto1.getKontoNr() + " überwiesen");
                        System.out.println(konto1.getSaldo());

                    } else if (konto.getKontoNr() != transferNr && transferNr == konto2.getKontoNr()) {
                        konto2.einzahlen(money);
                        System.out.println(money + " CHF an Konto " + konto2.getKontoNr() + " überwiesen");
                        System.out.println(konto2.getSaldo());

                    } else if (konto.getKontoNr() != transferNr && transferNr == konto3.getKontoNr()) {
                        konto3.einzahlen(money);
                        System.out.println(money + " CHF an Konto " + konto3.getKontoNr() + " überwiesen");
                        System.out.println(konto3.getSaldo());

                    } else {
                        System.out.println("Keine Transaktion möglich");
                    }

                    System.out.println("Eigener Kontostand: " + konto.getSaldo());

                    break;
                default:
                    duration = false;
                    break;
            }
        }
    }
}
