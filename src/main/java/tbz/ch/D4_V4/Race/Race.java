/*
 * Copyright (c) 2023-2023. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package tbz.ch.D4_V4.Race;

public abstract class Race {
    String name;
    int helf;
    int manoe;
    int streng;
    int anguel;

    public Race(String name, int helf, int manoe, int streng, int anguel) {
        this.name = name;
        this.helf = helf;
        this.manoe = manoe;
        this.streng = streng;
        this.anguel = anguel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHelf() {
        return helf;
    }

    public void setHelf(int helf) {
        this.helf = helf;
    }

    public void addHelf(int helf) {
        this.helf += helf;
    }

    public int getManoe() {
        return manoe;
    }

    public void setManoe(int manoe) {
        this.manoe = manoe;
    }

    public void addManoe(int manoe) {
        this.manoe += manoe;
    }

    public int getStreng() {
        return streng;
    }

    public void setStreng(int streng) {
        this.streng = streng;
    }

    public void addStreng(int streng) {
        this.streng += streng;
    }

    public int getAnguel() {
        return anguel;
    }

    public void setAnguel(int anguel) {
        this.anguel = anguel;
    }

    public void addAnguel(int anguel) {
        this.anguel += anguel;
    }

}



