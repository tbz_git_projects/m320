package tbz.ch.V3_StockExchange;

public class Aktie {

    float Wert;
    String Name;

    public Aktie(float wert, String name) {
        Wert = wert;
        Name = name;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public float getWert() {
        return Wert;
    }

    public void setWert(float wert) {
        Wert = wert;
    }
}
