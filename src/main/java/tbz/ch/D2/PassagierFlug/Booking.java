package tbz.ch.D2.PassagierFlug;

import java.util.Scanner;

/**
 * @author Robin & Tim
 */



public class Booking {

    /**
     * This is a program for adding a passgier or more to a Fligz.
     * * @param args
     */
    public static void main(String[] args) {

          /**
         * This is the main method
         * which is very important for
         * execution for a java program.
         */

        boolean duration = true;
        Flug myFlug = new Flug();

        System.out.println("Wilkommen zur Flugbuchung");

        Scanner scan = new Scanner(System.in);
        System.out.println("Geben Sie Ihren Namen an");
        String name = scan.nextLine();

        Passagier passagier = new Passagier(name);

        System.out.println("Wollen sie diesen Flug buchen? y/N");
        String firstChoice = scan.nextLine();
        if (firstChoice.equals("y")) {
            myFlug.addPassagiere(passagier);
            myFlug.PassagierListeAusgeben();
            System.out.println("Sie wurden hinzugefügt");
        } else {
            System.out.println("You didn't get added");
        }


        while (duration) {

            System.out.println("Wollen sie weitere Namen hinzufügen? y/N");
            String secondChoice = scan.nextLine();
            if (secondChoice.equalsIgnoreCase("y")) {

                System.out.println("Geben Sie einen Namen ein");
                String names = scan.nextLine();
                passagier = new Passagier(names);
                myFlug.addPassagiere(passagier);
                myFlug.PassagierListeAusgeben();

            } else {
                System.out.println("Vielen Dank für die Buchung, auf wiedersehen!");
                duration = false;
            }

        }


    }
}
